const apiKey = 'd8fb0add';
const baseURL = `http://www.omdbapi.com/?apikey=${apiKey}`;

const apiRequest = async (requestString) => {
  let response = await fetch(`${baseURL}${requestString}`);
  let data = await response.json();
  return data;
};

export default {
  methods: {
    
    searchRequest({ title, page = 1 }) {
      return apiRequest(`&s=${title}&page=${page}`);
    },
    
    idRequest(imdbID) {
      return apiRequest(`&i=${imdbID}`);
    },

  },
};