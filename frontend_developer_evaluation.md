# Frontend Developer Evaluation
Thank you for your interest! Below is a summary of what we'd like to see delivered. We're looking for an MVP to see how you approach designing and building an app from scratch. Good luck!

## Getting Started
1. Create a front end web app for the Open Movie Database 
2. Get an API key from: https://www.omdbapi.com/
3. Create a 2 page app: Search and History
     - Search page results should contain some of the data returned from the API (consider what is most important)
     - Use a local storage mechanism to store searches (denote difference between successful and failed searches)
4. Profit!

## Deliver
- Commit final work to preferred VCS as a public repo and send us the link for review

## Evaluation
`These are the things we will be looking at when evaluating your project`

- Design and styling
- Code layout and structure
- Use of resources
